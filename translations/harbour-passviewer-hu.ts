<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu_HU">
<context>
    <name>FirstPage</name>
    <message>
        <source>No passes found</source>
        <translation>Nem találtam kártyákat a készüléken</translation>
    </message>
    <message>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <source>Show</source>
        <translation>Megjelenítés</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation>Törlés folyamatban</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>Frissítés</translation>
    </message>
    <message>
        <source>pass update successful</source>
        <translation>Kártya sikeresen frissítve</translation>
    </message>
    <message>
        <source>pass not updateable</source>
        <translation>Nem sikerült a kártyát frissíteni</translation>
    </message>
    <message>
        <source>no new version for pass</source>
        <translation>Nincs új verzió a kártyából</translation>
    </message>
    <message>
        <source>pass update failed</source>
        <translation>Nem sikerült a kártyát frissíteni</translation>
    </message>
    <message>
        <source>Format Error</source>
        <translation>Formátumhiba</translation>
    </message>
    <message>
        <source>Couldn&apos;t recognize date/time format</source>
        <translation>Ismeretlen dátum/időformátum</translation>
    </message>
    <message>
        <source>Unsupported</source>
        <translation>Nem támogatott</translation>
    </message>
    <message>
        <source>Please update your system or install calendar</source>
        <translation>Kérlek frissítsd a rendszered vagy telepítsd a Naptár alkalmazást</translation>
    </message>
    <message>
        <source>Create Calendar Entry</source>
        <translation>Naptárbejegyzés létrehozása</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Time</source>
        <translation>Időpont</translation>
    </message>
    <message>
        <source>Highlight passes close to event time</source>
        <translation>Hamarosan kezdődőek kiemelése</translation>
    </message>
    <message>
        <source>Time before event</source>
        <translation>Ennyi idővel a kezdés előtt</translation>
    </message>
    <message>
        <source>Time after event</source>
        <translation>Ennyi ideig</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>Távolság</translation>
    </message>
    <message>
        <source>Highlight passes close to destination</source>
        <translation>Közeli események kiemelése</translation>
    </message>
    <message>
        <source>Distance to destination</source>
        <translation>Távolság a helyszínig</translation>
    </message>
    <message>
        <source>Allow passes to override distance</source>
        <translation>Távolság számítása a kártyák adatai alapján</translation>
    </message>
    <message>
        <source>Barcode</source>
        <translation>Vonalkód</translation>
    </message>
    <message>
        <source>Show barcode fullscreen when tapped</source>
        <translation>Vonalkódra koppintás után teljes képernyőn mutassa azt</translation>
    </message>
    <message>
        <source>Prefer non-satellite position fixing</source>
        <translation>Nem műhold alapú pozícionálás előnyben részesítése</translation>
    </message>
    <message>
        <source>Saves battery. &lt;b&gt;Requires&lt;/b&gt; &amp;quot;Faster position fix&amp;quot; switched on in the system settings.</source>
        <translation>Kíméli az akkumulátort. Használatához az &amp;quot;Energiatakarékos mód&amp;quot; beállításnak be kell lennie kapcsolva a Rendszerbeállítások &gt; GPS és Helymeghatározás menüben.</translation>
    </message>
</context>
<context>
    <name>ShowBack</name>
    <message>
        <source>Simple View</source>
        <translation>Egyszerű nézet</translation>
    </message>
</context>
<context>
    <name>ShowPass</name>
    <message>
        <source>Fullscreen Barcode</source>
        <translation>Teljesképernyős vonalkód</translation>
    </message>
    <message>
        <source>Simple View</source>
        <translation>Egyszerű nézet</translation>
    </message>
    <message>
        <source>Create Calendar Entry</source>
        <translation>Naptárbejegyzés létrehozása</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>Frissítés</translation>
    </message>
</context>
<context>
    <name>ShowSimple</name>
    <message>
        <source>Fullscreen Barcode</source>
        <translation>Teljesképernyős vonalkód</translation>
    </message>
</context>
</TS>
